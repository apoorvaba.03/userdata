
import Card from '../UI/Card';
import classes from './UsersList.module.css';
// importing the card 
//listing the user data using map
// we can used this in adduser but for modularity used in app.js 

const UsersList = (props) => {
    return(
        <Card className={classes.users}>
        <ul>
            {props.users.map(
                (user) => (
                    <li key={user.id}>{user.name} ({user.age} of year old.)</li>
                )
            )}
        </ul>
        </Card>
    );
};
export default UsersList;
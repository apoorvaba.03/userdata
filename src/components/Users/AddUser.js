import React, {useState} from 'react';
import Button from '../UI/Button';
import Card from '../UI/Card';
import ErrorModal from '../UI/ErrorModal'
import classes from './AddUser.module.css';
// using the resuable button component
// in order to store user enetred data used useSate 
// to close the overlay use lifting state method

const AddUser = (props) => {

    const [enterdUserName,setEnterdUserName]=useState('')
    const [enterdAge,setEnterdAge]=useState('');
    // error state to make overllay run when is error
    const [error,setError] = useState('');

    // after submit to store  the inputs keystroke prsent in states variable and passed that to outside method.
    // clear the fields after submission
    // field validation after submisiion
    const addUserHandler = (event) => {
        event.preventDefault();
        if(enterdUserName.trim().length === 0  || enterdAge.trim().length === 0)
        {
            setError(
                {
                    title:'Invalid Input',
                    message:'please enter the valid name and age(non-empty values)',
                }
            );
            return;
        }
        if(+enterdAge < 1)
        {
            setError({
                title:'invalid age',
                message:'please enter positive age',
            });
            return;
        }
       // console.log(enterdUserName,enterdAge);
       props.onAddUser(enterdUserName,enterdAge);
        setEnterdUserName('');
        setEnterdAge('');
    };;

    // to store keystroke value on input field...
    const userNameChangeHandler = (event) => {
        setEnterdUserName(event.target.value);

    };
    const ageChangeHandler = (event) => {
        setEnterdAge(event.target.value);
    }

    const errorHandler = () => {
        setError(null);
    };

    return(
        <div>
      {error &&  <ErrorModal title = {error.title} 
      message={error.message}
       onConfirm={errorHandler}/>}
        <Card className={classes.input}>
            
        <form  onSubmit={addUserHandler}>
            <label htmlFor="username">UserName:</label>
            <input id="username" type="text" onChange={userNameChangeHandler} value={enterdUserName}/>
            <label htmlFor="age">Age</label>
            <input id="age" type="number" onChange={ageChangeHandler} value={enterdAge} />
            <Button type="submit">submit</Button>
        </form>
        </Card>
        </div>


    );

}
export default AddUser;
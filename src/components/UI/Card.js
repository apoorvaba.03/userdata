import classes from './Card.module.css';
// wraping the content within the card and `` for adding outside style bcz ts is userdefind components
const Card = (props) => {
    return(
        <div className={`${classes.card} ${props.className}`}>
            {props.children}
        </div>
    );
};
export default Card;
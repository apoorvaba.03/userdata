import React from 'react';
import Card from './Card';
import Button from './Button';
import classes from './ErrorModal.module.css';
// reusing the button and card components
// error occur in adduser so need to import there
// backdrop to avoid interacting outside the overlay when error ocured.

const ErrorModal = (props) =>{
    return(
        <div>
            <div className={classes.backdrop} onClick={props.onConfirm}/>
        <Card className={classes.modal}>
            <header className={classes.header}>
                <h2>{props.title}</h2>
            </header>
            <div className={classes.content}>
                <p>{props.message}</p>
            </div>
            <footer className={classes.actions}>
            <Button onClick={props.onConfirm}>Okay</Button>
            </footer>

        </Card>
        </div>
    );
}
export default ErrorModal;
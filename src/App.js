
import './App.css';
import {useState} from 'react';
import AddUser from './components/Users/AddUser';
import UsersList from './components/Users/UsersList';
// passing userdata array to userlist
// to store adduser dataarray in app.js use useState   and pass that stored dataarray into usersList. 

function App() {
  const [usersList,setUsersList]= useState([]);
  const addUserHandler = (uName,uAge) => {
    setUsersList((prevUsersList) =>
    {
      return [...prevUsersList,{name:uName,age:uAge , id:Math.random().toString()}];
    }
    );
  };

  return (
    <div>
      <AddUser onAddUser = {addUserHandler} />
      <UsersList users={usersList} />
    </div>
  );
}

export default App;
